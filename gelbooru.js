/*
Gelbooru API getter by @Nikide
*/
const request = require("request");
const parseString = require('xml2js').parseString;
//Get Post by ID
exports.getById = function(id,callback){
	request(`https://gelbooru.com/index.php?page=dapi&s=post&q=index&id=${id}`, function (error, response, body) {
		parseString(body, function (err, result) {
			callback(result);
		});
	});
}
//Get Posts by Tags
exports.getByTags = function(tags,limit,pid,callback){
	request(`https://gelbooru.com/index.php?page=dapi&s=post&q=index&tags=${tags}&limit=${limit}&pid=${pid}`, function (error, response, body) {
		parseString(body, function (err, result) {
			callback(result);
		});
	});
}
//Get Random by Tags
exports.getRandom = function(tags,callback){
	request(`https://gelbooru.com/index.php?page=dapi&s=post&q=index&tags=${tags}&limit=1`, function (error, response, body) {
		parseString(body, function (err, result) {
			console.log(result);
			let random = Math.floor(Math.random() * result.posts.$.count) + 1;
			if(random < 1){
				random == random - 1;
			}
			request(`https://gelbooru.com/index.php?page=dapi&s=post&q=index&tags=${tags}&limit=1&pid=${random}`, function (error, response, body) {
				parseString(body, function (err, result) {
					console.log(result);
					callback(result);
				});
			});
		});
	});
}
