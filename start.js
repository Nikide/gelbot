const TeleBot = require('telebot');
const gel = require('./gelbooru.js');
const db = require('./db.js');
const fs = require('fs');
const https = require('https');

const BUTTONS = {
    nextpage: {
        label: 'Next page',
        command: '/nextpage'
    },
    random: {
        label: 'Random post',
        command: '/torandom'
	},
    newsearch: {
        label: 'New search',
        command: '/s'
	},
    newsearchr: {
        label: 'New random search',
        command: '/r'
	},
    jump: {
        label: 'Jump to page',
        command: '/jumptopage'
	},
    append: {
        label: 'Append',
        command: '/appendsearch'
	},
	appendr: {
        label: 'Appеnd', //random
        command: '/appendsearchrandom'
	},
	csearch: {
        label: 'Normal search',
        command: '/csearch'
	},
	test: {
        label: 'test',
        command: '/test'
	},
	user: {
        label: 'ℹ️',
        command: '/user'
    }
};

const bot = new TeleBot({
    token: '!',
    usePlugins: ['commandButton','askUser','namedButtons'],
     polling: { // Optional. Use polling.
        proxy: '!' // Optional. An HTTP proxy to be used.
     },
	pluginConfig: {
		namedButtons: {
            buttons: BUTTONS
        }
}});

const errorMessage = ['⛔Error while loading content⛔','Sorry, you can press button only once per 5 seconds ⏰'];
//!Special functions
const userMarkup = bot.inlineKeyboard([[bot.inlineButton('History', {callback:'/gethistory'}),bot.inlineButton('Feedback', {callback:'/feedback'})],[bot.inlineButton('Donate', {callback:'/donate'}),bot.inlineButton('Rate bot', {url:'https://telegram.me/storebot?start=gelbooru_bot'})]]);
const userMarkup2 = bot.inlineKeyboard([[bot.inlineButton('Select', {callback:'/sendnumber'}),bot.inlineButton('Feedback', {callback:'/feedback'})],[bot.inlineButton('Donate', {callback:'/donate'}),bot.inlineButton('Rate bot', {url:'https://telegram.me/storebot?start=gelbooru_bot'})]]);
const userList = new Map();
const callbackLimit = 1;
const imageLimit = 5;

function doWebm(url,fid,callback){
	let file_name = './webm/'+fid+'.webm';
	let file = fs.createWriteStream(file_name);
	https.get(url, function(response) {
		response.pipe(file);
		file.on('finish', function() {
			file.close();
			callback(file_name);
		});
	});
}

//!End

//!Functions
function sendImage(msg,limit){
	db.getSearch(msg.from.id,function(row){
		console.log(row);
		search = "-webm+"+row.search.toLowerCase().replace(/ /g,"+");
		gel.getByTags(''+search,limit,row.page,function(res){
			let endcount = Number(res.posts.$.offset) + limit;
			console.log(res);
			if(res.posts.$.count != 0 ){
				if(res.posts.$.count != res.posts.$.offset || typeof res.posts.post[0] == 'undefined'){
					let amount = Math.floor(Number(res.posts.$.count) / limit);
					let tagsmark = [];
					console.log(res);
					let sendpost = 0;
					let postString = res.posts.post;
					if(postString.indexOf('.webm')+1){
						bot.sendMessage(msg.from.id,'<b>Your response has WebM! Please wait, while content uploads on Telegram servers! This can take longer!<b>',{parseMode:'html'});
					}
					for (var i = 0; i < res.posts.post.length; i++) {
						var gelb = res.posts.post[i].$;
						console.log(gelb);
						let replyMarkup = bot.inlineKeyboard([[bot.inlineButton('Post №'+gelb.id+'', {callback:'{"c":"gin","id":"'+gelb.id+'"}'})]]);
						if(gelb.file_url.indexOf('.gif')+1){
							bot.sendAction(msg.from.id,'upload_video');
							bot.sendVideo(msg.from.id,gelb.file_url,{replyMarkup})
							.then(function(){
								sendpost++;
								if(sendpost == res.posts.post.length){
									bot.sendAction(msg.from.id,'typing');
									if(res.posts.$.count <= endcount){
										let replyMarkup = bot.keyboard([[BUTTONS.newsearch.label]],{resize:true});
										bot.sendMessage(msg.from.id,'You are on <b>last page</b>\n<b>Search: </b><code>'+row.search+'</code>',{replyMarkup,parseMode:'html'});
									}else{
										let replyMarkup = bot.keyboard([[BUTTONS.jump.label,BUTTONS.nextpage.label],[BUTTONS.newsearch.label,BUTTONS.append.label,BUTTONS.random.label,BUTTONS.user.label]],{resize:true});
										bot.sendMessage(msg.from.id,'You are on page <b>'+row.page+'</b> of <b>'+amount+'</b>\n<b>Search: </b><code>'+row.search+'</code>\n@nikidev',{parseMode:'html',replyMarkup});
									}
								}
							})
							.catch(function(reason) { //On error
								console.log(reason);
								console.log(gelb.file_url);
								sendpost++;
								bot.sendVideo(msg.from.id,gelb.sample_url,{replyMarkup}).then(function(){
									if(sendpost == res.posts.post.length){
										bot.sendAction(msg.from.id,'typing');
										if(res.posts.$.count <= endcount){
											let replyMarkup = bot.keyboard([[BUTTONS.newsearch.label]],{resize:true});
											bot.sendMessage(msg.from.id,'You are on <b>last page</b>\n<b>Search: </b><code>'+row.search+'</code>',{replyMarkup,parseMode:'html'});
										}else{
											let replyMarkup = bot.keyboard([[BUTTONS.jump.label,BUTTONS.nextpage.label],[BUTTONS.newsearch.label,BUTTONS.append.label,BUTTONS.random.label,BUTTONS.user.label]],{resize:true});
											bot.sendMessage(msg.from.id,'You are on page <b>'+row.page+'</b> of <b>'+amount+'</b>\n<b>Search: </b><code>'+row.search+'</code>\n@nikidev',{parseMode:'html',replyMarkup});
										}
									}
								});
							});

						}else if(gelb.file_url.indexOf('.webm')+1){ //webm
							bot.sendAction(msg.from.id,'upload_video');
							doWebm(gelb.file_url,gelb.id,function(file){

									bot.sendDocument(msg.from.id,file,{replyMarkup,caption:"Telegram not supporting Webm. Please download file and play it on your device"})
									.then(function(){
										sendpost++;
										fs.unlink(file);
										if(sendpost == res.posts.post.length){
											bot.sendAction(msg.from.id,'typing');
											if(res.posts.$.count <= endcount){
												let replyMarkup = bot.keyboard([[BUTTONS.newsearch.label]],{resize:true});
												bot.sendMessage(msg.from.id,'You are on <b>last page</b>\n<b>Search: </b><code>'+row.search+'</code>',{replyMarkup,parseMode:'html'});
											}else{
												let replyMarkup = bot.keyboard([[BUTTONS.jump.label,BUTTONS.nextpage.label],[BUTTONS.newsearch.label,BUTTONS.append.label,BUTTONS.random.label,BUTTONS.user.label]],{resize:true});
												bot.sendMessage(msg.from.id,'You are on page <b>'+row.page+'</b> of <b>'+amount+'</b>\n<b>Search: </b><code>'+row.search+'</code>\n@nikidev',{parseMode:'html',replyMarkup});
											}
										}
									})
									.catch(function(reason) { //On error
										console.log(reason);
										console.log(gelb.file_url);
										sendpost++;
										fs.unlink(file);
										bot.sendMessage(msg.from.id,'Webm upload error!\nhttps:'+gelb.sample_url,{replyMarkup}).then(function(){
											if(sendpost == res.posts.post.length){
												bot.sendAction(msg.from.id,'typing');
												if(res.posts.$.count <= endcount){
													let replyMarkup = bot.keyboard([[BUTTONS.newsearch.label]],{resize:true});
													bot.sendMessage(msg.from.id,'You are on <b>last page</b>\n<b>Search: </b><code>'+row.search+'</code>',{replyMarkup,parseMode:'html'});
												}else{
													let replyMarkup = bot.keyboard([[BUTTONS.jump.label,BUTTONS.nextpage.label],[BUTTONS.newsearch.label,BUTTONS.append.label,BUTTONS.random.label,BUTTONS.user.label]],{resize:true});
													bot.sendMessage(msg.from.id,'You are on page <b>'+row.page+'</b> of <b>'+amount+'</b>\n<b>Search: </b><code>'+row.search+'</code>\n@nikidev',{parseMode:'html',replyMarkup});
												}
											}
										});
									});

							});
						}else{
							bot.sendAction(msg.from.id,'upload_photo');
							bot.sendPhoto(msg.from.id,gelb.file_url,{replyMarkup})
							.then(function(){
								sendpost++;
								if(sendpost == res.posts.post.length){
									bot.sendAction(msg.from.id,'typing');
									if(res.posts.$.count <= endcount){
										let replyMarkup = bot.keyboard([[BUTTONS.newsearch.label]],{resize:true});
										bot.sendMessage(msg.from.id,'You are on <b>last page</b>\n<b>Search: </b><code>'+row.search+'</code>',{replyMarkup,parseMode:'html'});
									}else{
										let replyMarkup = bot.keyboard([[BUTTONS.jump.label,BUTTONS.nextpage.label],[BUTTONS.newsearch.label,BUTTONS.append.label,BUTTONS.random.label,BUTTONS.user.label]],{resize:true});
										bot.sendMessage(msg.from.id,'You are on page <b>'+row.page+'</b> of <b>'+amount+'</b>\n<b>Search: </b><code>'+row.search+'</code>\n@nikidev',{parseMode:'html',replyMarkup});
									}
								}
							})
							.catch(function(reason) { //On error
								console.log(reason);
								sendpost++;
								bot.sendMessage(msg.from.id,errorMessage[0]+'\nTEC: '+reason.error_code);
								if(sendpost == res.posts.post.length){
									bot.sendAction(msg.from.id,'typing');
									if(res.posts.$.count <= endcount){
										let replyMarkup = bot.keyboard([[BUTTONS.newsearch.label]],{resize:true});
										bot.sendMessage(msg.from.id,'You are on <b>last page</b>\n<b>Search: </b><code>'+row.search+'</code>',{replyMarkup,parseMode:'html'});
									}else{
										let replyMarkup = bot.keyboard([[BUTTONS.jump.label,BUTTONS.nextpage.label],[BUTTONS.newsearch.label,BUTTONS.append.label,BUTTONS.random.label,BUTTONS.user.label]],{resize:true});
										bot.sendMessage(msg.from.id,'You are on page <b>'+row.page+'</b> of <b>'+amount+'</b>\n<b>Search: </b><code>'+row.search+'</code>\n@nikidev',{parseMode:'html',replyMarkup});
									}
								}
							});
						}
				}
				let newpage = Number(row.page) + 1;
				db.updatePage(msg.from.id,newpage);
			}else{
				bot.sendSticker(msg.from.id,'CAADAgAD_AEAArzR-QvBpFWENFfQ2gI').then(function(){
					bot.sendMessage(msg.from.id,'<b>No pages!</b>',{parseMode:'html'});
				});
			}
		}else{
			bot.sendSticker(msg.from.id,'CAADBAADYwADHc-4B8XmmVlo71fiAg').then(function(){
				let replyMarkup = bot.keyboard([[BUTTONS.newsearch.label]],{resize:true});
				bot.sendMessage(msg.from.id,"I can't find something with <i>'"+row.search+"'</i>\nDo you think, your request is correct?",{replyMarkup,parseMode:'html'});
			});
		}
		});
	});
}

function sendInfo(msg,id,tags,taco){
	db.getSearch(msg.from.id,function(row){
		gel.getById(id,function(res){
			console.log(res.posts.post[0].$);
			let tagsmark = [];
			let tagsinl = res.posts.post[0].$.tags.trim().split(/\s+/);
			const [chatId, messageId] = [msg.from.id,msg.message.message_id];
			console.log('Message: '+msg.message.message_id);
			tagsmark.push([bot.inlineButton('On Gelbooru', {url:'https://gelbooru.com/index.php?page=post&s=view&id='+res.posts.post[0].$.id})]);
			tagsinl.forEach(function(item, i, arr) {
					tagsmark.push([bot.inlineButton(item, {callback:'{"c":"a","t":"'+item+'"}'})]);
					console.log('Added: '+item+' = '+i+'Taco: '+taco);
			});
			let replyMarkup = bot.inlineKeyboard(tagsmark);
			console.log(JSON.stringify(replyMarkup));
			bot.editMessageCaption({chatId, messageId}, '', {replyMarkup})
			.catch(err => {
				let tagsmark = [];
				tagsinl.forEach(function(item, i, arr) {
					tagsmark.push([bot.inlineButton(item, {callback:'{"c":"us","t":"'+item+'"}'})]);
				});
				let replyMarkup = bot.inlineKeyboard(tagsmark);
				console.log(err);
				bot.editMessageCaption({chatId, messageId}, '', {replyMarkup});
			});
		});
	});
}

function tagAction(msg,tag){
	let replyMarkup = bot.inlineKeyboard([[bot.inlineButton('Search', {callback:'{"c":"s","t":"'+tag+'"}'}),bot.inlineButton('Append', {callback:'{"c":"us","t":"'+tag+'"}'})],[bot.inlineButton('Random', {callback:`/r ${tag}`})]]);
	const [chatId, messageId] = [msg.from.id,msg.message.message_id];
	bot.editMessageCaption({chatId, messageId}, 'Select action for "'+tag+'"', {replyMarkup})
        .catch(err => {
            console.log(err);
            bot.editMessageCaption({chatId, messageId}, 'Tags action error');
        });
}


function sendRandom(msg,us){
	db.getSearch(msg.from.id,function(row){
		search = ""+row.search.toLowerCase().replace(/ /g,"+");
		if(us != 1){
			var replyMarkup = bot.keyboard([[BUTTONS.random.label],[BUTTONS.newsearch.label,BUTTONS.newsearchr.label,BUTTONS.appendr.label,BUTTONS.user.label]],{resize:true});
		}else{
			var replyMarkup = bot.keyboard([[BUTTONS.random.label],[BUTTONS.newsearch.label,BUTTONS.newsearchr.label,BUTTONS.appendr.label,BUTTONS.user.label]],{resize:true});
		}
		bot.sendMessage(msg.from.id,'Random post with <code>'+row.search+'</code>\n<b>Please, wait...</b>',{replyMarkup,parseMode:'html'})
		.then(lMsg => {
      console.log(lMsg);
			const [chatId, messageId] = [msg.from.id,lMsg.message_id];
			gel.getRandom(''+search,function(res){
				console.log(res);
				if(res.posts.$.count != 0 || typeof res.posts.post != "undefined"){
					try{
						var gelb = res.posts.post[0].$;
						if(gelb.file_url.indexOf('.gif')+1){
							bot.sendAction(msg.from.id,'upload_video');
							let replyMarkup = bot.inlineKeyboard([[bot.inlineButton('Post №'+gelb.id+'', {callback:'{"c":"gin","id":"'+gelb.id+'"}'})]]);
							bot.sendVideo(msg.from.id,gelb.file_url,{replyMarkup})
							.then(res=>{

							})
							.catch(err=>{
								console.log(err);
								bot.sendAction(msg.from.id,'upload_video');
								let replyMarkup = bot.inlineKeyboard([[bot.inlineButton('Post №'+gelb.id+'', {callback:'{"c":"gin","id":"'+gelb.id+'"}'})]]);
								bot.sendVideo(msg.from.id,gelb.sample_url,{replyMarkup});
							});

							}else if(gelb.file_url.indexOf('.webm')+1){ //webm
								bot.sendPhoto(msg.from.id,gelb.preview_url,{caption:'This is preview for WebM! Please wait...'}).then(function(){
									bot.sendAction(msg.from.id,'upload_video');
									doWebm(gelb.file_url,gelb.id,function(file){
										let replyMarkup = bot.inlineKeyboard([[bot.inlineButton('Post №'+gelb.id+'', {callback:'{"c":"gin","id":"'+gelb.id+'"}'})]]);
										bot.sendDocument(msg.from.id,file,{replyMarkup,caption:"Telegram not supporting Webm. Please download file and play it on your device"})
										.then(function(){
											fs.unlink(file);
										})
										.catch(function(reason) { //On error
											fs.unlink(file);
											bot.sendMessage(msg.from.id,'Webm upload error!\nhttps:'+gelb.sample_url,{replyMarkup}).then(function(){
											});
										});
									});
								});
						}else{
							bot.sendAction(msg.from.id,'upload_photo');
							let replyMarkup = bot.inlineKeyboard([[bot.inlineButton('Post №'+gelb.id+'', {callback:'{"c":"gin","id":"'+gelb.id+'"}'})]]);
							bot.sendPhoto(msg.from.id,gelb.file_url,{replyMarkup})
							.then(res=>{

							})
							.catch(err=>{
								console.log(err);
								bot.sendMessage(msg.from.id,errorMessage[0]);
							});
						}
					}catch(e){
						bot.sendMessage(msg.from.id,'⛔ Gelbooru API error! ⛔');
					}
				}else{
					bot.sendSticker(msg.from.id,'CAADBAADYwADHc-4B8XmmVlo71fiAg').then(function(){
						let replyMarkup = bot.keyboard([[BUTTONS.newsearchr.label]],{resize:true});
						bot.sendMessage(msg.from.id,"I can't find something with <i>'"+row.search+"'</i>\nDo you think, your request is correct?",{replyMarkup,parseMode:'html'});
					});
				}
			});
		});
	});
}
//!End

//!I/O

bot.on('*', msg => {
  	bot.sendMessage(msg.from.id,"<b>Warning! Bot working unstable!</b>\nPlease, wait for updates",{parseMode:'html'});
	db.chekBase(function(status){
		if(status == 0){
			bot.sendMessage(msg.from.id,"<b>Warning! Database connection failed!</b>\nIf you see this message too long, contact @nikide",{parseMode:'html'});
		}
	});
});

bot.on('/start', msg => {
	bot.sendSticker(msg.from.id,'CAADBAAD1QADOOEaAAFaMC9zxp8gSwI').then(function(){
			let replyMarkup = bot.keyboard([[BUTTONS.newsearch.label]],{resize:true});
			bot.sendMessage(msg.from.id,'<b>Gelbooru Bot</b> by @nikidev\n/s - Search\n/help - Help ¯\_(ツ)_/¯\n/feedback - Feedback',{replyMarkup,parseMode:'html'});
	});
});

bot.on('/s', msg => {
		bot.sendSticker(msg.from.id,'CAADBAAD2AADOOEaAAEJjwABFRfOQnoC').then(function(){
			bot.sendMessage(msg.from.id,"I'm waiting for your tags. Please, send me their!",{ask:'search',replyMarkup: 'hide'});
		});
});
bot.on('/jumptopage', msg => {
		bot.sendSticker(msg.from.id,'CAADBAADAQIAAjjhGgABueDyTY35rkUC').then(function(){
			bot.sendMessage(msg.from.id,"Okay! Send me number of page, what you want to jump!",{ask:'jump',replyMarkup: 'hide'});
		});
});
bot.on('ask.jump', msg => {
	let page = Number(msg.text);
	if(Number.isInteger(page)){
		db.updatePage(msg.from.id,msg.text,function(){
			bot.sendMessage(msg.from.id,`Jump to page <b>${page}</b>!`,{parseMode:'html'}).then(res =>{
				sendImage(msg,imageLimit);
			});

		});
	}else{
		bot.sendMessage(msg.from.id,'Please, send number of page!',{ask:"jump"});
	}
});

bot.on('/nextpage', msg => {
	sendImage(msg,imageLimit);
});
bot.on('/donate', msg => {
	bot.sendSticker(msg.from.id,'CAADAgADmgEAArzR-QtT-q-BsOVV-AI').then(function(){
		let replyMarkup = bot.inlineKeyboard([[bot.inlineButton('Donate via Yandex.Money', {url:'https://yasobe.ru/na/nikidev'})]]);
		bot.sendMessage(msg.from.id,'Sempai, i need your help!',{replyMarkup});
	});
});
bot.on('/appendsearch', msg => {
	db.getSearch(msg.from.id,function(row){
		bot.sendSticker(msg.from.id,'CAADAgADnAEAArzR-Qs_o4hOevzdPQI').then(function(){
			let replyMarkup = bot.keyboard([['animated','anal','animated_gif'],['uncensored']], {resize: true});
			bot.sendMessage(msg.from.id,'You now search <b>'+row.search+'</b>\nWrite tags or select, what you want append to search!',{ask:'append',parseMode:'html',replyMarkup});
		});
	});
});
bot.on('/appendsearchrandom', msg => {
	db.getSearch(msg.from.id,function(row){
		bot.sendSticker(msg.from.id,'CAADAgADnAEAArzR-Qs_o4hOevzdPQI').then(function(){
			let replyMarkup = bot.keyboard([['animated','anal','animated_gif'],['uncensored']], {resize: true});
			bot.sendMessage(msg.from.id,'You now search <b>'+row.search+'</b>\nWrite tags or select, what you want append to search!',{ask:'appendr',parseMode:'html',replyMarkup});
		});
	});
});
bot.on('ask.append', msg => {
	db.getSearch(msg.from.id,function(row){
		let newsearch = row.search+' '+msg.text;
		db.updateSearch(msg.from.id,newsearch,function(){
				sendImage(msg,imageLimit);
		});
	});
});
bot.on('ask.appendr', msg => {
	db.getSearch(msg.from.id,function(row){
		let newsearch = row.search+' '+msg.text;
		db.updateSearch(msg.from.id,newsearch,function(){
				sendRandom(msg,0);
		});
	});
});
bot.on('ask.search', msg => {
	if(msg.text.indexOf('/')+1){
		bot.sendSticker(msg.from.id,'CAADAgADlAEAArzR-Qu0UkMbd-y5AQI').then(function(){
			bot.sendMessage(msg.from.id,"Oh... Can you retry your request?",{ask:'search',replyMarkup: 'hide'});
		});
	}else{
		db.updateSearch(msg.from.id,msg.text,function(){
			sendImage(msg,imageLimit);
		});
	}
});
bot.on(['photo','sticker','video','document'], msg => {
	bot.sendSticker(msg.from.id,'CAADAgAD2QEAArzR-QsFaseYE2enoAI').then(function(){
		bot.sendSticker(msg.from.id,'CAADAgAD2wEAArzR-QsCicESpzR7rQI');
	});
});
bot.on('/help', msg => {
	bot.sendSticker(msg.from.id,'CAADBAAD7gADOOEaAAHNKhVwnuFPOQI').then(function(){
		bot.sendMessage(msg.from.id,'Use /s for search all hentai things what you want\nWrite tags in /s like "loli" or if you want animated loli write "loli animated", if you want only gif without webm write "loli animated_gif". Also all spaces is _  "hatsune miku" is "hatsune_miku"!\n/r is random post\n',{parseMode:'html'});
	});
});

bot.on('/csearch',msg =>{
	sendImage(msg,imageLimit);
});

bot.on('/r', msg => {
		bot.sendSticker(msg.from.id,'CAADBAAD2AADOOEaAAEJjwABFRfOQnoC').then(function(){
			bot.sendMessage(msg.from.id,"I'm waiting for your tags. Please, send me their!",{ask:'random',replyMarkup: 'hide'});
		});
});
bot.on('/torandom', msg => {
	sendRandom(msg,1);
});
/*
	Feedback
*/
bot.on('/getfb', msg =>{
	bot.sendMessage(msg.from.id,fs.readFileSync('fb.txt','utf8'));
});
bot.on('/feedback', msg =>{
	bot.sendMessage(msg.from.id,'@nikide');
});
bot.on('/user', msg =>{
	let replyMarkup = userMarkup;
	db.getSearch(msg.from.id,function(row){
		bot.sendSticker(msg.from.id,'CAADAgADWgEAArzR-QvF7BBtzUqbzwI').then(function(){
			db.getStatUsers(function(all){
				db.getStatSearchUser(msg.from.id,function(userSearch){
					db.getStatSearchAll(function(allSearch){
						bot.sendMessage(msg.from.id,'Hello, <b>'+msg.from.first_name+'</b>! Last search <code>'+row.search+'</code>\nTotal users of bot: <b>'+all+'</b>\nYou searched <b>'+userSearch+'</b> times\nAll users searched <b>'+allSearch+'</b> times\nHave nice fap, Goshujinsama 😘',{parseMode:'html',replyMarkup});
					})
				})
			})
		});
	});
});
//bot.sendMessage(msg.from.id,'Hello, <b>'+msg.from.first_name+'</b>! Last search <code>'+row.search+'</code>',{parseMode:'html',replyMarkup});
bot.on('/gethistory',msg=>{
	let replyMarkup = userMarkup2;
	console.log(msg);
	const [chatId, messageId] = [msg.from.id,msg.message.message_id];
	db.getHistory(msg.from.id,function(row){
		let history;
		console.log(row);
		let toMessage = '';
		for (var i = 0; i < row.length; i++) {
			history = row[i];
			let number = i + 1;
			toMessage += '<b>'+number+'</b> - <i>'+history.search+'</i>\n';
		}
		bot.editMessageText({chatId, messageId},'History is recorded when you are searching or appending tags\n<b>Your last 10 search history:</b>\n'+toMessage,{parseMode:'html',replyMarkup});
	});
});
bot.on('/sendnumber',msg=>{
	let replyMarkup = bot.keyboard([['1','2','3','4','5'],['6','7','8','9','10']], {resize: true});
	bot.sendMessage(msg.from.id,"Please, select number from history list!",{ask:'historynum',replyMarkup});
});
bot.on('ask.historynum', msg => {
	let num = Number(msg.text) - 1;
	db.getHistory(msg.from.id,function(row){
		if(!Number.isInteger(num)){
			bot.sendMessage(msg.from.id,"Please, send correct number!",{ask:'historynum'});
		}else if(Number(msg.text) > row.length){
			bot.sendMessage(msg.from.id,"Please, send correct number!",{ask:'historynum'});
		}else if(num < 0){
			bot.sendMessage(msg.from.id,"Please, send correct number!",{ask:'historynum'});
		}else{
			db.updateSearch(msg.from.id,row[num].search,function(){
				let replyMarkup = bot.keyboard([[BUTTONS.csearch.label,BUTTONS.random.label]], {resize: true});
				bot.sendMessage(msg.from.id,'You are selected <b>'+row[num].search+'</b> Please, select action...',{parseMode:'html',replyMarkup}).then(res=>{
				});
			});
		}

	});
});
bot.on('ask.feedback', msg => {
	fs.appendFileSync('fb.txt', 'From: '+msg.from.id+' Message: '+msg.text+'\n');
	bot.sendMessage(msg.from.id,'Thanks you for feedback!');
});
/*
	End
*/
bot.on('/getstat', msg =>{
	db.getStatUsers(function(res){
		bot.sendMessage(msg.from.id,'Total users '+res);
	});
});
bot.on('ask.random', (msg, props) => {
	db.updateSearch(msg.from.id,msg.text,function(){
		sendRandom(msg);
	});
});

bot.on('callbackQuery', msg => {
	bot.answerCallbackQuery(msg.id,'Loading...',true);
    let data = JSON.parse(msg.data);

	let now;
	let currentTime = Math.floor(new Date() / 1000);
	if(userList.has(msg.from.id) == false){
		userList.set(msg.from.id,currentTime);
		now = callbackLimit + 1;
	}else{
		 now = Number(currentTime) - Number(userList.get(msg.from.id));
	}

	if(now <= callbackLimit){ // on limit
		bot.sendMessage(msg.from.id,errorMessage[1]);
	}else{
		userList.set(msg.from.id,currentTime); //Update limit
		if(data.c == 'gin'){
			sendInfo(msg,data.id);
		}else if(data.c == 'n'){
			page = Number(data.id) + 1;
			sendImage(msg,data.t,2,page);
		}else if(data.c == 's'){
			db.updateSearch(msg.from.id,data.t,function(){
				sendImage(msg,imageLimit);
			});
		}
		else if(data.c == 'us'){
			db.getSearch(msg.from.id,function(row){
				let newsearch = row.search+' '+data.t;
				db.updateSearch(msg.from.id,newsearch,function(){
					sendImage(msg,imageLimit);
				});
			});
		}
		else if(data.c == 'random'){
			sendRandom(msg,data.tags);
		}
		else if(data.c == 'a'){
			tagAction(msg,data.t);
		}else{
			bot.sendMessage(msg.from.id,'function deleted\n/start /help /s /r');
		}
	}
    console.log('callbackQuery data:', data.c);
    bot.answerCallbackQuery(msg.id);
});
//!End
/*bot.on('inlineQuery', msg => {

    let query = msg.query;
    console.log(`inline query: ${ query }`);
    // Create a new answer list object
    const answers = bot.answerList(msg.id, {cacheTime: 60});
    search = query.toLowerCase().replace(/ /g,"+");
    // Article
    gel.getByTags('-webm+'+search,10,0,function(res){
            for (var i = 0; i < res.posts.post.length; i++) {
                var gelb = res.posts.post[i].$;
                answers.addPhoto({
                id: 'photo',
                photo_url: gelb.file_url,
                thumb_url: gelb.file_url
                });
            }
            bot.answerQuery(answers).catch(err=>{console.log(err);});
    });

    // Send answers

});*/

bot.start();
