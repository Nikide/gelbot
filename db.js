var r = require('rethinkdbdash')({
	port: 28015,
	host: 'localhost'
});
//Первый раз~
r.dbCreate("gb").run();
r.db('gb').tableCreate("search").run();
r.db('gb').tableCreate("users").run();
r.db('gb').tableCreate("history").run(res=>console.log(res));
//Check
function isExist(table,what,search){
  r.db('gb').table(table).filter({uid:what}).run().then(res=>{
    if(typeof res[0] == 'undefined'){
      console.log('Not found:'+what);
      if(!search){
        search ='loli';
      }
      if(table == 'search'){
        r.db('gb').table(table).insert({
          uid: what,
          search: search,
          page: 0,
          limit:5
        }).run().then(res =>{
          console.log("Adding:"+what);
        });
      }else if(table == 'users'){
         r.db('gb').table(table).insert({
          uid: what,
          lang: "en",
          par1: "",
          par2: "",
          par3: "",
        }).run().then(res =>{
          console.log("Adding:"+what);
        });
      }else if(table == 'history'){
         r.db('gb').table(table).insert({
          uid: what,
          search: search,
          date: Math.floor(Date.now() / 1000)
        }).run().then(res =>{
          console.log("Adding:"+what);
        });
      }
    }else{
      console.log("Exist:"+what);
    }
  });

}

exports.getSearch = function(uid,callback){
  isExist('search',uid);
  r.db('gb').table('search').filter({uid:uid}).run().then(res=>{
    callback(res[0]);
  });
}
exports.getHistory = function(uid,callback){
  isExist('history',uid);
  r.db('gb').table('history').filter({uid:uid}).orderBy(r.desc('date')).limit(10).run().then(res=>{
    callback(res);
  });
}
exports.updateSearch = function(uid,update,callback){
  isExist('search',uid,update);
  r.db('gb').table('search').filter({uid:uid}).update({search:update,page:0}).run().then(res =>{
    r.db('gb').table('history').insert({
      uid:uid,
      search:update,
      date: Math.floor(Date.now() / 1000)
    }).run().then(res=>{callback(res)});
  });
}

exports.updatePage = function(uid,update,callback){
  isExist('search',uid);
  r.db('gb').table('search').filter({uid:uid}).update({page:update}).run().then(res =>{callback(res);});
}
exports.chekBase = function(callback){
  r.db('gb').table('search').run().then(res =>{callback(1);}).catch(e=>{callback(0);console.log('1111111111111111111111111111111111111')});
}
//r.db('gb').tableCreate('history').run().then(res=>{console.log(res)});
exports.getStatUsers = function(callback){
  r.db('gb').table('search').count().run().then(res =>{
   callback(res);
  });
}
exports.getStatSearchAll = function(callback){
  r.db('gb').table('history').count().run().then(res =>{
   callback(res);
  });
}
exports.getStatSearchUser = function(uid,callback){
  r.db('gb').table('history').filter({uid:uid}).count().run().then(res =>{
   callback(res);
  });
}
